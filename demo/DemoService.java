package demo;

import service.*;

public class DemoService {
    private String text = Text.text;
    public void execute() {
	showResult(new ServiceText());
    }

    private void showResult(IServiceText service) {
    	System.out.println("Word count: ");
    	System.out.println(service.counter(text).toString());
    	System.out.println();

    	System.out.println("Unique: ");
    	System.out.println(service.unique(text).toString());
    	System.out.println();

    	System.out.println("Sort: ");
    	System.out.println(service.sort(text).toString());
    }
}
