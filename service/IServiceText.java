package service;

import java.util.*;
import java.io.*;

public interface IServiceText {
    Map<String, Long> counter(String file);
    Set<String> unique(String file);
    ArrayList<String> sort(String file);
}
