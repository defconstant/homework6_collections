package service;

import java.util.*;
import java.io.*;

public class ServiceText implements IServiceText {
    public Map<String,Long> counter(String str) {
	Map<String,Long> map = new HashMap<>();
	String delimiter = "[\\p{Punct}\\p{Blank}\\n]+";
	for (String s : str.split(delimiter)) {
	    if (map.containsKey(s))
		map.put(s, map.get(s) + 1);
	    else
		map.put(s, 1L);
	}
	return map;
    }

    public Set<String> unique(String str) {
	Set<String> set = new HashSet<String>();
	String delimiter = "[\\p{Punct}\\p{Blank}\\n]+";
	for (String s : str.split(delimiter)) {
	    set.add(s);
	}
	return set;
    }

    public ArrayList<String> sort(String str) {
	ArrayList<String> ar = new ArrayList<String>();
	String delimiter = "[\\p{Punct}\\p{Blank}\\n]+";
	for (String s : str.split(delimiter)) {
	    ar.add(s);
	}
	Collections.sort(ar, new SortString());
	return ar;
    }
}

class SortString implements Comparator<String> {
    public int compare(String a, String b) {
	return a.compareTo(b);
    }
}
